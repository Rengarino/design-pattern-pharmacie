package model;

import java.time.LocalDate;

public class Pharmacien extends Employe {
	
	private double salaire;
	
    public Pharmacien(String nom, String prenom, String adresse, double salaire) {
        super(nom, prenom, adresse);
        this.salaire = salaire;
    }

	@Override
	public double getSalaire(double CA, double nbHeures) {
		return salaire * (CA * 0.01) ;
	}
}
