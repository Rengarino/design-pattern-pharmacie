package model;

import model.abstractFactory.CarteMastercardFactory;
import model.abstractFactory.CarteVisaFactory;
import model.abstractFactory.PaysFactory;
import model.abstractFactoryCommerce.*;
import model.bridge.Mastercard;
import model.bridge.Visa;

import java.time.LocalDate;
import java.time.Month;

public class Mainmodel {
    public static void main(String[] args) {
        Responsable valentin = new Responsable("Valentin", "Parrilla", "100 Rue Raimbaud", 1500.50);
        CompteBancaireClassique compteSG = new CompteBancaireClassique("Société Générale", "12345123412");
        PharmacieFactory pharmacieFactory = PharmacieFactory.getInstance();
        PaysFactory pf = new PaysFactory();
        pf.createPays("France", 67000, 2.771, 0);
        pf.createPays("Etats-Unis", 100000, 50.483, 0.20);
        pf.createPays("Espagne", 80000, 2.550, 0.10);
        pf.createPays("Portugal", 50000, 1.000, 0.15);
        pf.createPays("Royaume-Uni", 80000, 3.000, 0.30);

        PharmacieIndependante pharmacieDuBonheur = pharmacieFactory.createCommerceIndependant("Pharmacie du Bonheur", 200, "12345678911234", valentin, compteSG);
        pharmacieDuBonheur.displayPharmacieInfos();
        PreparateurDeCommande l = new PreparateurDeCommande("L", "L", "Osef", 10.15, LocalDate.of(2014, 10, 15));
        pharmacieDuBonheur.addEmploye(l);
        pharmacieDuBonheur.displayPharmacieInfos();
        //Pharmacie Franchisée
        CompteBancaireClassique compteBNP = new CompteBancaireClassique("BNP", "12345123412");
        CompteBancaireFranchise compteFranchiseTest = new CompteBancaireFranchise("Test", "1234567890");
        PharmacieFranchiseeMere pharmacieDuCheneMere = pharmacieFactory.createCommerceFranchiseeMereSansMere("Pharmacie du Chene", 300, "12345678911234", valentin, compteBNP, compteFranchiseTest);
        Responsable caroline = new Responsable("Caroline", "Chaibainou", "51 Rue Des Pastèques", 1470);
        PharmacieFranchisee pharmacieDuCheneBeaumes = pharmacieFactory.createCommerceFranchiseeMere("Pharmacie du Chene", 250, "12345678911234", caroline, compteBNP,  pharmacieDuCheneMere, compteFranchiseTest);
        pharmacieDuCheneBeaumes.displayPharmacieInfos();
        pharmacieDuCheneMere.addFille(pharmacieDuCheneBeaumes);
        pharmacieDuCheneMere.addFille(pharmacieFactory.createCommerceFranchisee("Pharmacie Test", 250, "12345678911234", caroline, compteBNP, compteFranchiseTest, pharmacieDuCheneMere));
        pharmacieDuCheneMere.addFille(pharmacieFactory.createCommerceFranchisee("Pharmacie Test 2", 250, "12345678911234", caroline, compteBNP, compteFranchiseTest, pharmacieDuCheneMere));

        //Test stock sur PharmacieIndependante
        ProduitPharmaceutique vaccin1 = new ProduitPharmaceutique("Vaccin1", "Vaccins", 25.50, LocalDate.of(2040, Month.APRIL, 20));
        pharmacieDuBonheur.addToStock(vaccin1, 4, 40.5);
        pharmacieDuBonheur.compteBancaireClassique.approvisionner(5000);
        pharmacieDuBonheur.addToStock(vaccin1, 2, 40.5); //Les produits se stack si ils sont les mêmes
        ProduitPharmaceutique doliprane = new ProduitPharmaceutique("Doliprane", "Médicament", 8.5, LocalDate.of(2060, Month.APRIL, 12));

        //Client carte visa
        CarteVisaFactory cvf = new CarteVisaFactory();
        Visa visa = new Visa();
        Client client = new Client("Toto", "Toto", "250 Chemin de la Garonne", compteBNP, cvf.getCarteClassique("Toto", "Toto", LocalDate.now(), 1234, visa, compteBNP));
        System.out.println("Cout par transaction de carte Visa : " +client.getCarteBancaire().getReseauBancaire().getCoutReseau());
        //Client carte mastercard
        CarteMastercardFactory cmf = new CarteMastercardFactory();
        Mastercard mastercard = new Mastercard();
        CompteBancaireClassique compteBNP2 = new CompteBancaireClassique("BNP2", "12345123412");
        Client client2 = new Client("Toto", "Toto", "250 Chemin de la Garonne", compteBNP2, cmf.getCarteClassique("Toto", "Toto", LocalDate.now(), 1234, mastercard, compteBNP2));
        System.out.println("Cout par transaction de carte Mastercard : " +client2.getCarteBancaire().getReseauBancaire().getCoutReseau());
        //Achat de produit par client visa
        System.out.println("Achat de produit par client Visa");
        client.acheter(pharmacieDuBonheur.getFromStock("Vaccin1", 2));
        client.getCompteBancaireClassique().approvisionner(500);
        //Test d'un cas limite ou le client acheterais plus de produits que dans le stock (peut-être via internet ?)
        client.acheter(pharmacieDuBonheur.getFromStock("Vaccin1", 5));
        //Retour à un cas réel
        client.acheter(pharmacieDuBonheur.getFromStock("Vaccin1", 4));
        System.out.println("");
        //Achat de produit par client mastercard
        pharmacieDuCheneMere.addToStock(doliprane, 10, 11.5);
        System.out.println("");
        client2.getCompteBancaireClassique().approvisionner(300);
        client2.acheter(pharmacieDuCheneMere.getFromStock("Doliprane", 4));
    }
}
