package model;

import java.time.LocalDate;

public class ProduitPharmaceutique {
    private String nom;
    private String type;
    private double prixAchat;
    private double prixVente;
    private LocalDate datePeremption;
    private int stock;

    public ProduitPharmaceutique(String nom, String type, double prixAchat, LocalDate datePeremption) {
        this.nom = nom;
        this.type = type;
        this.prixAchat = prixAchat;
        this.datePeremption = datePeremption;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getPrixAchat() {
        return prixAchat;
    }

    public void setPrixAchat(double prixAchat) {
        this.prixAchat = prixAchat;
    }

    public double getPrixVente() {
        return prixVente;
    }

    public void setPrixVente(double prixVente) {
        this.prixVente = prixVente;
    }

    public LocalDate getDatePeremption() {
        return datePeremption;
    }

    public void setDatePeremption(LocalDate datePeremption) {
        this.datePeremption = datePeremption;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public int getStock() {
        return this.stock;
    }

    public void increaseStock(int stock) {
        this.stock+= stock;
    }

    public boolean decreaseStock(int stock) {
        if(this.stock - stock < 0) return false;
        return true;
    }
}
