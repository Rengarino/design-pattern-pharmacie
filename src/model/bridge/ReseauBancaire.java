package model.bridge;

import model.ProduitPharmaceutique;

import java.util.List;

public abstract class ReseauBancaire {
    private double coutReseau = 0.0;

    public double transaction(List<ProduitPharmaceutique> listProduitPharmaceutique) {
        double prixCommande = 0.0;
        for(ProduitPharmaceutique pf : listProduitPharmaceutique) {
            prixCommande+=pf.getPrixVente();
        }
        return prixCommande * coutReseau;
    }

    public abstract void remboursement();

    public double getCoutReseau() {
        return coutReseau;
    }

    public void setCoutReseau(double coutReseau) {
        this.coutReseau = coutReseau;
    }
}
