package model;

public class MementoCompteBancaire {
	
	double solde;
	
	public MementoCompteBancaire(double solde) {
		this.solde = solde;
	}

	public double getSolde() {
		return solde;
	}
	
}
