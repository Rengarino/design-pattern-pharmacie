package model;

import java.util.ArrayList;

public class StockPharmacie {
    public ArrayList<ProduitPharmaceutique> listArticle;
    public StockPharmacie() {
        this.listArticle = new ArrayList<>();
    }

    public void addArticle(ProduitPharmaceutique produitPharmaceutique, int nbToAdd) {
        //Si il n'y a aucun article on l'ajoute
        if(this.listArticle.size() == 0) {
            this.listArticle.add(produitPharmaceutique);
            this.listArticle.get(0).increaseStock(nbToAdd);
        }
        else {
            for (ProduitPharmaceutique pp : this.listArticle) {
                if (pp.getNom().equals(produitPharmaceutique.getNom()) && pp.getDatePeremption().equals(produitPharmaceutique.getDatePeremption())) {
                    pp.increaseStock(nbToAdd); //On augmente le stock de l'article déjà existant;
                } else {
                    produitPharmaceutique.setStock(nbToAdd); //Sinon on initialise un nouvel article
                    this.listArticle.add(produitPharmaceutique); //Et on l'ajoute à la liste des articles
                }
            }
        }
    }

    public double getFromStock(String nom, int nb) {
        double prixAPayer = 0.0;
        for(ProduitPharmaceutique pp : this.listArticle) {
            if (pp.getNom().equals(nom)) {
                if(pp.decreaseStock(nb)) {
                   prixAPayer = pp.getPrixVente() * nb;
                }
                else {
                    System.out.println("Il n'y a plus assez de ce produit en stock");
                }
            }
        }
        return prixAPayer;
    }
}
