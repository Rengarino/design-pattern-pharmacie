package model;

import model.abstractFactory.CarteBancaire;

public class Client {
    private String nom;
    private String prenom;
    private String adresse;
    private CompteBancaireClassique compteBancaireClassique;
    private CarteBancaire cb;

    public Client(String nom, String prenom, String adresse, CompteBancaireClassique compteBancaireClassique, CarteBancaire cb) {
        this.nom = nom;
        this.prenom = prenom;
        this.adresse = adresse;
        this.compteBancaireClassique = compteBancaireClassique;
        this.cb = cb;
    }
    
    public CompteBancaireClassique getCompteBancaireClassique() {
    	return this.compteBancaireClassique;
    }

    public CarteBancaire getCarteBancaire() {
        return this.cb;
    }

    public void acheter(double somme) {
        if(somme > 0.0) this.cb.payer(somme);
    }

}
