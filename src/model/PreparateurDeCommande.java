package model;

import java.time.LocalDate;
import java.util.Date;

public class PreparateurDeCommande extends Employe {
	
	private double tauxHoraire;
	private LocalDate entree;
	
    public PreparateurDeCommande(String nom, String prenom, String adresse, double tauxHoraire, LocalDate entree) {
        super(nom, prenom, adresse);
        this.tauxHoraire = tauxHoraire;
        this.entree = entree;
    }
    
    public double getTauxHoraire() {
    	return this.tauxHoraire;
    }
    
	@Override
	public double getSalaire(double CA, double nbHeures) {
		LocalDate anciennete = LocalDate.from(this.entree);
		double salaire = nbHeures * tauxHoraire;
		if(anciennete.compareTo(LocalDate.of(3, 0, 0)) < 0) {
			return salaire;
		}
		else if(anciennete.compareTo(LocalDate.of(6, 0, 0)) < 0) {
			salaire += salaire * 0.10;
			return salaire;
		}
		else {
			salaire += salaire * 0.15;
			return salaire;
		}
	}
}
