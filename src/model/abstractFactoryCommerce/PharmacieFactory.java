package model.abstractFactoryCommerce;

import model.*;

public final class PharmacieFactory implements ICommerceFactory {

    private PharmacieFactory() {
        // La présence d'un constructeur privé supprime le constructeur public par défaut.
        // De plus, seul le singleton peut s'instancier lui-même.
        super();
    }
    private static PharmacieFactory instance = null;

    public final static PharmacieFactory getInstance() {
        if(PharmacieFactory.instance == null)
        {
            PharmacieFactory.instance = new PharmacieFactory();
        }
        return PharmacieFactory.instance;
    }

    @Override
    public PharmacieIndependante createCommerceIndependant(String nom, double surface, String SIRET, Responsable responsable, CompteBancaireClassique compteBancaireClassique) {
        return new PharmacieIndependante(nom, surface,  SIRET, responsable, compteBancaireClassique);
    }

    @Override
    public PharmacieFranchiseeFille createCommerceFranchisee(String nom, double surface, String SIRET, Responsable responsable, CompteBancaireClassique compteBancaireClassique, CompteBancaireFranchise compteBancaireFranchise, Commerce commerceFranchiseMere) {
        return new PharmacieFranchiseeFille(nom, surface, SIRET, responsable, compteBancaireClassique, compteBancaireFranchise, (PharmacieFranchiseeMere) commerceFranchiseMere);
    }

    @Override
    public PharmacieFranchiseeMere createCommerceFranchiseeMere(String nom, double surface, String SIRET, Responsable responsable, CompteBancaireClassique compteBancaireClassique, Commerce commerceFranchiseMere, CompteBancaireFranchise compteBancaireFranchise) {
        return new PharmacieFranchiseeMere(nom, surface, SIRET, responsable, compteBancaireClassique, (PharmacieFranchiseeMere) commerceFranchiseMere, compteBancaireFranchise);
    }

    @Override
    public PharmacieFranchiseeMere createCommerceFranchiseeMereSansMere(String nom, double surface, String SIRET, Responsable responsable, CompteBancaireClassique compteBancaireClassique, CompteBancaireFranchise compteBancaireFranchise) {
        return new PharmacieFranchiseeMere(nom, surface, SIRET, responsable, compteBancaireClassique, compteBancaireFranchise);
    }
}
