package model.abstractFactoryCommerce;

import model.CompteBancaireClassique;
import model.Responsable;

public abstract class Commerce {
    private String nom;
    private double surface;
    private String SIRET;
    public CompteBancaireClassique compteBancaireClassique;
    private Responsable responsable;

    public Commerce(String nom, double surface, String SIRET, Responsable responsable, CompteBancaireClassique compteBancaireClassique) {
        this.nom = nom;
        this.surface = surface;
        this.SIRET = SIRET;
        this.responsable = responsable;
        this.compteBancaireClassique = compteBancaireClassique;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public double getSurface() {
        return surface;
    }

    public void setSurface(double surface) {
        this.surface = surface;
    }

    public String getSIRET() {
        return SIRET;
    }

    public void setSIRET(String SIRET) {
        this.SIRET = SIRET;
    }

    public Responsable getResponsable() {
        return responsable;
    }

    public void setResponsable(Responsable responsable) {
        this.responsable = responsable;
    }

}
