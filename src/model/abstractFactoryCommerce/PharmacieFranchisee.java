package model.abstractFactoryCommerce;

import model.CompteBancaireClassique;
import model.CompteBancaireFranchise;
import model.ProduitPharmaceutique;
import model.Responsable;

public abstract class PharmacieFranchisee extends Pharmacie {
    private CompteBancaireFranchise compteBancaireFranchise;
    PharmacieFranchiseeMere pharmacieFranchiseeMere;

    public PharmacieFranchisee(String nom, double surface, String SIRET, Responsable responsable, CompteBancaireClassique compteBancaireClassique, CompteBancaireFranchise compteBancaireFranchise) {
        super(nom, surface, SIRET, responsable, compteBancaireClassique);
        this.pharmacieFranchiseeMere = pharmacieFranchiseeMere;
        this.compteBancaireFranchise = compteBancaireFranchise;
    }
    
    public void addToStockFranchisee(ProduitPharmaceutique produitPharmaceutique, int nbToAdd, double prixVente) {
        produitPharmaceutique.setPrixVente(prixVente); //On met le prix de vente que la pharmacie à décidé de mettre
        double resteApresRemise = 1 - pharmacieFranchiseeMere.getRemiseAchat();
        if(this.compteBancaireFranchise.debiter(produitPharmaceutique.getPrixAchat()*nbToAdd *resteApresRemise)) this.stock.addArticle(produitPharmaceutique, nbToAdd);
    }
}
