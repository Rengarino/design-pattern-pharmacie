package model.abstractFactoryCommerce;

import model.CompteBancaireClassique;
import model.CompteBancaireFranchise;
import model.Responsable;

import java.util.ArrayList;

//Au final PharmacieFranchiseeMere extends Pharmacie car une PharmacieFranchiseeMere peut avoir une PharmacieFranchiseeMere ou non.
public class PharmacieFranchiseeMere extends PharmacieFranchisee {
    ArrayList<PharmacieFranchisee> listPharmacieFille;
    double pourcentageChiffreFilles;
    double pourcentageRemiseAchat;
    public CompteBancaireFranchise compteBancaireFranchise;
    private int nbFranchisees;

    //Constructeur avec mere
    public PharmacieFranchiseeMere(String nom, double surface, String SIRET, Responsable responsable, CompteBancaireClassique compteBancaireClassique, PharmacieFranchiseeMere pharmacieFranchiseeMere, CompteBancaireFranchise compteBancaireFranchise) {
        super(nom, surface, SIRET, responsable, compteBancaireClassique, compteBancaireFranchise);
        this.pharmacieFranchiseeMere = pharmacieFranchiseeMere;
        this.listPharmacieFille = new ArrayList<PharmacieFranchisee>();
        this.compteBancaireFranchise = compteBancaireFranchise;
        this.nbFranchisees = this.listPharmacieFille.size();
    }

    //Constructeur sans mere
    public PharmacieFranchiseeMere(String nom, double surface, String SIRET, Responsable responsable, CompteBancaireClassique compteBancaireClassique, CompteBancaireFranchise compteBancaireFranchise) {
        super(nom, surface, SIRET, responsable, compteBancaireClassique, compteBancaireFranchise);
        this.listPharmacieFille = new ArrayList<PharmacieFranchisee>();
        this.compteBancaireFranchise = compteBancaireFranchise;
        this.pharmacieFranchiseeMere = null; //La mère la plus haute
        this.nbFranchisees = this.listPharmacieFille.size();
    }
    
    //toAdd sera égal à 1 quand on ajoute une fille
    public int incrementFranchisees() {
    	if(this.pharmacieFranchiseeMere == null) {
    		return this.nbFranchisees;
    	}
    	this.nbFranchisees++;
    	return this.nbFranchisees + this.pharmacieFranchiseeMere.incrementFranchisees();
    }

    public void addFille(PharmacieFranchisee pharmacieFranchisee) {
        this.listPharmacieFille.add(pharmacieFranchisee);
        this.nbFranchisees++;
        this.incrementFranchisees();
        System.out.println("nombre de filles : " + this.nbFranchisees);
        setRemises(); //On met à jour les taux après ajout
    }
    
    public double getRemiseAchat() {
    	return this.pourcentageRemiseAchat;
    }

    public void setRemises() {
        if(this.nbFranchisees >= 2 && this.nbFranchisees <= 4) {
            this.pourcentageChiffreFilles = 0.01;
            this.pourcentageRemiseAchat = 0.025;
        }

        else if(this.nbFranchisees >= 5 && this.nbFranchisees <= 10) {
            this.pourcentageChiffreFilles = 0.02;
            this.pourcentageRemiseAchat = 0.05;
        }

        else if(this.nbFranchisees > 10) {
            this.pourcentageChiffreFilles = 0.03;
            this.pourcentageRemiseAchat = 0.075;
        }

        else return;
    }

    public void displayFilles() {
        for(PharmacieFranchisee pf : listPharmacieFille) {
            System.out.println("Pharmacie sous ma responsabilité : " + pf.getNom());
        }
    }
}
