package model.abstractFactoryCommerce;

import model.CompteBancaireClassique;
import model.CompteBancaireFranchise;
import model.Responsable;

public class PharmacieFranchiseeFille extends PharmacieFranchisee {

    PharmacieFranchiseeMere pharmacieFranchiseeMere;
    public PharmacieFranchiseeFille(String nom, double surface, String SIRET, Responsable responsable, CompteBancaireClassique compteBancaireClassique, CompteBancaireFranchise compteBancaireFranchise, PharmacieFranchiseeMere pharmacieFranchiseeMere) {
        super(nom, surface, SIRET, responsable, compteBancaireClassique, compteBancaireFranchise);
        this.pharmacieFranchiseeMere = pharmacieFranchiseeMere;
    }
}
