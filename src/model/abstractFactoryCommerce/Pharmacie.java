package model.abstractFactoryCommerce;

import model.*;
import model.abstractFactoryCommerce.Commerce;

import java.util.HashMap;

public abstract class Pharmacie extends Commerce {
    private int nbEmploye;
    private HashMap<Employe, Double> listEmployes;
    protected StockPharmacie stock;

    public Pharmacie(String nom, double surface, String SIRET, Responsable responsable, CompteBancaireClassique compteBancaireClassique) {
        super(nom, surface, SIRET, responsable, compteBancaireClassique);
        this.nbEmploye = 0;
        this.listEmployes = new HashMap<>();
        this.stock = new StockPharmacie();
    }

    public void addEmploye(Employe employe) {
        this.listEmployes.put(employe, (double) 0);
        this.nbEmploye++;
    }

    //Lors de l'ajout du produit au stock on donne le ProduitPharmaceutique (ou l'article) que l'on souhaite ajouté et son nombre.
    //On regarde si on a la possibilité de payer, si oui on ajoute le produit à son stock.
    public void addToStock(ProduitPharmaceutique produitPharmaceutique, int nbToAdd, double prixVente) {
        produitPharmaceutique.setPrixVente(prixVente); //On met le prix de vente que la pharmacie à décidé de mettre
        if(this.compteBancaireClassique.debiter(produitPharmaceutique.getPrixAchat()*nbToAdd)) this.stock.addArticle(produitPharmaceutique, nbToAdd);
    }

    public double getFromStock(String nom, int nb) {
        return this.stock.getFromStock(nom , nb);
    }

    public int getNbEmploye() {
        return nbEmploye;
    }

    public double getCA() {
    	// TODO : calculer le chiffre d'affaire en fonction des ventes
    	return 3000;
    }

    public void displayPharmacieInfos() {
        System.out.println(this.getNom() + " Surface : " + this.getSurface() + "m²");
        System.out.println("N° SIRET : " + this.getSIRET() + "\n Responsable : " + this.getResponsable().getPrenom() + " " + this.getResponsable().getNom() + "\n Nombre d'employe : " + this.getNbEmploye());
    }
    
    public void paiementEmployes() {
    	for(Employe e : listEmployes.keySet()) {
    		this.compteBancaireClassique.debiter(e.getSalaire(this.getCA(), (double) listEmployes.get(e)));
    	}
    }

}
