package model.abstractFactoryCommerce;

import model.CompteBancaireClassique;
import model.Responsable;

public class PharmacieIndependante extends Pharmacie {

    public PharmacieIndependante(String nom, double surface, String SIRET, Responsable responsable, CompteBancaireClassique compteBancaireClassique) {
        super(nom, surface, SIRET, responsable, compteBancaireClassique);
    }
}
