package model.abstractFactoryCommerce;

import model.*;

public interface ICommerceFactory {
    public Commerce createCommerceIndependant(String nom, double surface, String SIRET, Responsable responsable, CompteBancaireClassique compteBancaireClassique);
    public Commerce createCommerceFranchisee(String nom, double surface, String SIRET, Responsable responsable, CompteBancaireClassique compteBancaireClassique, CompteBancaireFranchise compteBancaireFranchise, Commerce commerceFranchiseMere);
    public Commerce createCommerceFranchiseeMere(String nom, double surface, String SIRET, Responsable responsable, CompteBancaireClassique compteBancaireClassique, Commerce commerceFranchiseMere, CompteBancaireFranchise compteBancaireFranchise);
    public Commerce createCommerceFranchiseeMereSansMere(String nom, double surface, String SIRET, Responsable responsable, CompteBancaireClassique compteBancaireClassique, CompteBancaireFranchise compteBancaireFranchise);
}
