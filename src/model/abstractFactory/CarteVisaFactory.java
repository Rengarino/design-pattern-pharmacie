package model.abstractFactory;

import model.CompteBancaire;
import model.bridge.ReseauBancaire;
import model.bridge.Visa;

import java.time.LocalDate;

public class CarteVisaFactory implements ICarteFactory {
    private Pays pays;

    @Override
    public CarteClassiqueVisa getCarteClassique(String nom, String prenom, LocalDate dateExpire, int code, ReseauBancaire reseauBancaire, CompteBancaire compteBancaire) {
        CarteClassiqueVisa cc = new CarteClassiqueVisa(nom, prenom, dateExpire, code, (Visa) reseauBancaire, compteBancaire);
        return cc;
    }

    @Override
    public CartePharmacieVisa getCartePharmacie(String nom, String prenom, LocalDate dateExpire, int code, String nomPharmacie, ReseauBancaire reseauBancaire, CompteBancaire compteBancaire) {
        CartePharmacieVisa cp = new CartePharmacieVisa(nom, prenom, dateExpire, code, nomPharmacie, (Visa) reseauBancaire, compteBancaire);
        return cp;
    }

    public double getCoutRetenue(Pays pays) {
        return this.pays.getCoutRetenue();
    }
}
