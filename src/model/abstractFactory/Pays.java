package model.abstractFactory;

public class Pays {
    private String name;
    private double population;
    private double PIB;
    private double coutRetenue;

    public Pays(String name, double population, double PIB, double coutRetenue) {
        this.name = name;
        this.population = population;
        this.PIB = PIB;
        this.coutRetenue = coutRetenue;
    }
    
    public String getName() {
    	return this.name;
    }
    
    public double getPopulation() {
    	return this.population;
    } 
    
    public double getPIB() {
    	return this.PIB;
    }

    public double getCoutRetenue() {
        return coutRetenue;
    }
}
