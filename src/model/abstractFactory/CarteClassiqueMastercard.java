package model.abstractFactory;

import model.CompteBancaire;
import model.bridge.Mastercard;

import java.time.LocalDate;

public class CarteClassiqueMastercard extends CarteClassique {
    public CarteClassiqueMastercard(String nom, String prenom, LocalDate dateExpire, int code, Mastercard mastercard, CompteBancaire compteBancaire) {
        super(nom, prenom, dateExpire, code, mastercard, compteBancaire);
    }
}
