package model.abstractFactory;

import model.CompteBancaire;
import model.bridge.ReseauBancaire;

import java.time.LocalDate;

public class CarteClassique extends CarteBancaire {
    public CarteClassique(String nom, String prenom, LocalDate dateExpire, int code, ReseauBancaire reseauBancaire, CompteBancaire compteBancaire) {
        super(nom, prenom, dateExpire, code, reseauBancaire, compteBancaire);
    }
}
