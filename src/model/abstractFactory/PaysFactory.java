package model.abstractFactory;

import java.util.ArrayList;

public class PaysFactory {
	
	private ArrayList<Pays> pays;
	
	public PaysFactory() {
		this.pays = new ArrayList<Pays>();
	}
	
	public Pays createPays(String name, double population, double PIB, double coutRetenue) {
		Pays pays = new Pays(name, population, PIB, coutRetenue);
		this.pays.add(pays);
		return pays;
	}
	
	public Pays getPays(String name) {
		for(Pays pays : this.pays) {
			if(pays.getName().equals(name)) {
				return pays;
			}
		}
		return null;
	}

}