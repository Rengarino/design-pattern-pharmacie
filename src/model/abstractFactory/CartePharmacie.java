package model.abstractFactory;

import model.CompteBancaire;
import model.bridge.ReseauBancaire;

import java.time.LocalDate;

public class CartePharmacie extends CarteBancaire {
    private String nomPharmacie;
    public CartePharmacie(String nom, String prenom, LocalDate dateExpire, int code, String nomPharmacie, ReseauBancaire reseauBancaire, CompteBancaire compteBancaire) {
        super(nom, prenom, dateExpire, code, reseauBancaire, compteBancaire);
        this.nomPharmacie = nomPharmacie;
    }
}
