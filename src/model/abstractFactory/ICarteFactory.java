package model.abstractFactory;

import model.CompteBancaire;
import model.bridge.ReseauBancaire;

import java.time.LocalDate;

public interface ICarteFactory {
    public CarteClassique getCarteClassique(String nom, String prenom, LocalDate dateExpire, int code, ReseauBancaire reseauBancaire, CompteBancaire compteBancaire);
    public CartePharmacie getCartePharmacie(String nom, String prenom, LocalDate dateExpire, int code, String nomPharmacie, ReseauBancaire reseauBancaire, CompteBancaire compteBancaire);
}
