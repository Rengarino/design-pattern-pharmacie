package model.abstractFactory;

import model.CompteBancaire;
import model.bridge.Mastercard;
import model.bridge.ReseauBancaire;

import java.time.LocalDate;

public class CarteMastercardFactory implements ICarteFactory {
    @Override
    public CarteClassiqueMastercard getCarteClassique(String nom, String prenom, LocalDate dateExpire, int code, ReseauBancaire reseauBancaire, CompteBancaire compteBancaire) {
        CarteClassiqueMastercard cc = new CarteClassiqueMastercard(nom, prenom, dateExpire, code, (Mastercard) reseauBancaire, compteBancaire);
        return cc;
    }

    @Override
    public CartePharmacieMastercard getCartePharmacie(String nom, String prenom, LocalDate dateExpire, int code, String nomPharmacie, ReseauBancaire reseauBancaire, CompteBancaire compteBancaire) {
        CartePharmacieMastercard cp = new CartePharmacieMastercard(nom, prenom, dateExpire, code, nomPharmacie, (Mastercard) reseauBancaire, compteBancaire);
        return cp;
    }
}
