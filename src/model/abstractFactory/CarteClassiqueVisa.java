package model.abstractFactory;

import model.CompteBancaire;
import model.bridge.Visa;

import java.time.LocalDate;

public class CarteClassiqueVisa extends CarteClassique {
    public CarteClassiqueVisa(String nom, String prenom, LocalDate dateExpire, int code, Visa visa, CompteBancaire compteBancaire) {
        super(nom, prenom, dateExpire, code, visa, compteBancaire);
    }
}
