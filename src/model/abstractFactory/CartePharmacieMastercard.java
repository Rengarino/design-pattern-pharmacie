package model.abstractFactory;

import model.CompteBancaire;
import model.bridge.Mastercard;

import java.time.LocalDate;

public class CartePharmacieMastercard extends CartePharmacie {
    public CartePharmacieMastercard(String nom, String prenom, LocalDate dateExpire, int code, String nomPharmacie, Mastercard mastercard, CompteBancaire compteBancaire) {
        super(nom, prenom, dateExpire, code, nomPharmacie, mastercard, compteBancaire);
    }
}
