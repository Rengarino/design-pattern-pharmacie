package model.abstractFactory;

import model.CompteBancaire;
import model.bridge.ReseauBancaire;

import java.time.LocalDate;

public abstract class CarteBancaire {
    private String nom, prenom;
    private LocalDate dateExpire;
    private int code;
    private ReseauBancaire reseauBancaire;
    private CompteBancaire compteBancaire;

    public CarteBancaire(String nom, String prenom, LocalDate dateExpire, int code, ReseauBancaire reseauBancaire, CompteBancaire compteBancaire) {
        this.nom = nom;
        this.prenom = prenom;
        this.dateExpire = dateExpire;
        this.code = code;
        this.reseauBancaire = reseauBancaire;
        this.compteBancaire = compteBancaire;
    }

    public ReseauBancaire getReseauBancaire() {
        return reseauBancaire;
    }

    public CompteBancaire getCompteBancaire() {
        return compteBancaire;
    }

    public boolean payer(double somme) {
        if(this.compteBancaire.debiter(somme + (somme *this.reseauBancaire.getCoutReseau()))) return true;
        return false;
    }

}