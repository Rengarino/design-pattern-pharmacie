package model.abstractFactory;

import model.CompteBancaire;
import model.bridge.Visa;

import java.time.LocalDate;

public class CartePharmacieVisa extends CartePharmacie {
    public CartePharmacieVisa(String nom, String prenom, LocalDate dateExpire, int code, String nomPharmacie, Visa visa, CompteBancaire compteBancaire) {
        super(nom, prenom, dateExpire, code, nomPharmacie, visa, compteBancaire);
    }
}
