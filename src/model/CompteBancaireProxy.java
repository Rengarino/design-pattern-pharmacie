package model;

import java.util.ArrayList;

public class CompteBancaireProxy implements ICompteBancaire {
	
	private CompteBancaire cb;

	public CompteBancaireProxy(CompteBancaire cb) {
        this.cb = cb;
    }
	
	public void approvisionner(double somme) {
        this.cb.approvisionner(somme);
    }

    public double getSolde() {
        return this.cb.getSolde();
    }

    public String getRib() {
        return this.cb.getRib();
    }
    
    public String getNomBanque() {
    	return this.cb.getNomBanque();
    }

    public boolean debiter(double somme) {
    	if(somme < this.cb.solde) {
    		return this.cb.debiter(somme);
    	}
    	return false;
    }
    
    public MementoCompteBancaire save() {
    	return this.cb.save();
    }
    
    public void restore(int i) {
    	this.cb.restore(i);
    }
}
