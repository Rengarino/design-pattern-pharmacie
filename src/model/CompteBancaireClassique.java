package model;

public class CompteBancaireClassique extends CompteBancaire {
	
	private CompteBancaireProxy proxy;
	
    public CompteBancaireClassique(String nomBanque, String RIB) {
        super(nomBanque, RIB);
    }
    
    public CompteBancaireProxy getProxy() {
    	return this.proxy;
    }
}
