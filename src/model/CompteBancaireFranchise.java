package model;

public class CompteBancaireFranchise extends CompteBancaire {
	
	private CompteBancaireProxy proxy;
	
    public CompteBancaireFranchise(String nomBanque, String RIB) {
        super(nomBanque, RIB);
    }
    
    public CompteBancaireProxy getProxy() {
    	return this.proxy;
    }
}
