package model;

import java.util.ArrayList;

public abstract class CompteBancaire implements ICompteBancaire {
    protected String RIB;
    protected double solde;
    protected String nomBanque;
    private ArrayList<MementoCompteBancaire> historique;

    public CompteBancaire(String nomBanque, String RIB) {
        this.nomBanque = nomBanque;
        this.RIB = RIB;
        this.solde = 0.0;
        historique = new ArrayList<MementoCompteBancaire>();
    }

    public void approvisionner(double somme) {
        this.solde+= somme;
        System.out.println("Votre nouveau solde est de : " +this.solde + " euros.");
    }

    public double getSolde() {
        return this.solde;
    }

    public String getRib() {
        return this.RIB;
    }
    
    public String getNomBanque() {
    	return this.nomBanque;
    }

    public boolean debiter(double somme) {
        if((this.solde - somme <= 0.0)) {
            System.out.println("Erreur somme demandée = " + somme + " et solde du compte = " + this.solde);
            System.out.println("Votre solde n'est pas assez important venez réapprovisionner le compte");
            return false;
        }
        this.solde-= somme;
        System.out.println("Votre solde vient d'être débité de la somme suivante : " + somme);
        System.out.println("Votre nouveau solde est de : " +this.solde + " euros.");
        return true;
    }
    
    public MementoCompteBancaire save() {
    	MementoCompteBancaire memento = new MementoCompteBancaire(this.solde);
    	this.historique.add(memento);
    	return memento;
    }
    
    public void restore(int i) {
    	MementoCompteBancaire memento = this.historique.get(i);
    	this.solde = memento.getSolde();
    }

}